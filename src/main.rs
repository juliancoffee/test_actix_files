use actix_files as fs;
use actix_files::NamedFile;
use actix_web::{get, App, HttpServer, Responder};
use actix_web::middleware::Logger;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    run_server().await
}

pub async fn run_server(
) -> std::io::Result<()> {
    log::info!("starting server");
    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .service(index)
//            .service(fs::Files::new("/static", "./static"))
    })
    .bind("127.0.0.1:8000")?
    .run()
    .await
}

#[get("/")]
pub async fn index() -> impl Responder {
    log::info!("index");
    NamedFile::open("./index.html")
}
